import { BaseElement } from '@bbva-commons-web-components/cells-base-elements';

class PersonDm extends BaseElement {
   
    static get properties() {
        return { };
    }

    async savePerson(person) {
        var cabeceras = new Headers();
        cabeceras.append("Content-Type", "application/json");
        var body = JSON.stringify(person);
        var requestOptions = {
            method: 'POST',
            headers: cabeceras,
            body: body
        };
        const result = await fetch("http://www.soft-dk.com:1337/personas", requestOptions).then(response => response.json());
        console.log('Result save persona', result);    
        return result;
    }

    async listPerson() {
        var requestOptions = {
            method: 'GET'
          };
         return await fetch("http://www.soft-dk.com:1337/personas", requestOptions).then(response => response.json());
    }
    
}
customElements.define('person-dm', PersonDm);