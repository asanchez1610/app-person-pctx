/* eslint-disable no-unused-vars */
import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`.content {
  padding: 15px;
  width: 100vw;
  height: 100vh;
  background-color: #ccc;
}

.dark {
  --header-bgcolor: #072146;
  --header-color: #fff;
  --header-btn-action-color-hover: #ddd7f7;
}

:host {
  background-color: #ccc;
}

cells-card-panel {
  background-color: #fff;
}

h2 {
  margin-top: 0;
  width: 100%;
  text-align: center;
  margin-bottom: 10px;
  border-bottom: 1px solid #666;
}

.body-content {
  height: calc(100vh - 120px);
}
`;