import { CellsPage } from '@cells/cells-page';
import { BbvaCoreIntlMixin } from '@bbva-web-components/bbva-core-intl-mixin';
import { html } from 'lit-element';
import '@cells-components/cells-template-paper-drawer-panel';
import styles from './main-page-styles.js';
import stylesGrid from '@bbva-commons-web-components/cells-base-elements/GridSystemCss';
import '@capa-cells/cells-person-pctx/cells-person-pctx';
import '@bbva-commons-web-components/cells-card-panel/cells-card-panel';
import '../../elements/dm/person-dm/person-dm';
import '@bbva-web-components/bbva-web-notification-toast/bbva-web-notification-toast';
import '@bbva-commons-web-components/cells-data-table/cells-data-table';

/* eslint-disable new-cap */
class LoginPage extends BbvaCoreIntlMixin(CellsPage) {
  static get is() {
    return 'main-page';
  }
  
  static get properties() {
    return { 
      items: Array,
      columns: Array
     };
  }

  constructor() {
    super();
    this.items = [];
    this.columns = [
      {
        title: 'ID',
        dataField: 'id',
        columnRender: (value, index, row) => {
          return value;
        },
      },
      {
        title: 'Nombres',
        dataField: 'nombres',
        columnRender: (value, index, row) => {
          return value;
        },
      },
      {
        title: 'Apellidos',
        dataField: 'apellidos',
        columnRender: (value, index, row) => {
          return value;
        },
      },
      {
        title: 'Tipo documento',
        dataField: 'tipoDocumento',
        columnRender: (value, index, row) => {
          return value;
        },
      },
      {
        title: 'Núimero documento',
        dataField: 'numeroDocumento',
        columnRender: (value, index, row) => {
          return value === 'Male' ? 'Masculino' : 'Femenino';
        },
      }
    ];
  }

  async onRegister(persona) {
    console.log('PageMain onregister', persona);
    let body = {};
    body.nombres = persona.nombres;
    body.apellidos = persona.apellidos;
    body.tipoDocumento = persona.tipoDocumento;
    body.numeroDocumento = persona.numDocumento;
    console.log('onRegister paylod', body);
    const response = await this.personDm.savePerson(body);
    console.log('response', response);
    this.shadowRoot.querySelector('bbva-web-notification-toast').opened = true;
    this.listPerson();
  }

  get personDm() {
    return this.shadowRoot.querySelector('person-dm');
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__main" class="content">
          <h2>Modulo de personas</h2>
          <div class="row content-panels">
            <div
              class="col-12	col-sm-5 col-md-5 col-lg-5 col-xl-5	col-xxl-5 col-section">

              <cells-card-panel class="dark body-content" headertitle="Registro de personas">
                <div slot="body">
                  <cells-person-pctx
                          @on-register-person="${({detail})=> this.onRegister(detail)}"
                          ></cells-person-pctx>
                </div>
              </cells-card-panel>
              
            </div>
            <div
              class="col-12	col-sm-7 col-md-7 col-lg-7 col-xl-7	col-xxl-7 col-section">
              <cells-data-table checkbox-selection="" 
              .columnsConfig="${this.columns}"
              .items="${this.items}" 
              id="data-table"></cells-data-table>
            </div>
          </div>
         <person-dm></person-dm>
         <bbva-web-notification-toast>
          <p>Persona registrada de forma correcta</p>
        </bbva-web-notification-toast>
        </div>
     </cells-template-paper-drawer-panel>`;
  }

  async listPerson() {
    this.items = await this.personDm.listPerson();
  }

  onPageEnter() {
    this.listPerson();
  }

  onPageLeave() {
  }

  static get styles() {
    return [styles, stylesGrid];
  }
}

window.customElements.define(LoginPage.is, LoginPage);